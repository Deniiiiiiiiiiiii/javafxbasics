import javafx.application.Application;

/**
 * Created: 09.04.2023
 *
 * @author Barbos Ioana-Denisa (barbo)
 */
public class Launcher {

    public static void main(String[] args) {
        //Application.launch(Basics_1.class, args);
        Application.launch(Basics_2.class, args);
    }
}
