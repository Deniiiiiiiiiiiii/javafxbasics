import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * Created: 09.04.2023
 *
 * @author Barbos Ioana-Denisa (barbo)
 */
public class Basics_1 extends Application {

    private RadioButton radioButton1;
    private RadioButton radioButton2;

    private TextField textField1;
    private TextField textField2;

    private TextField outputLabel;


    @Override
    public void start(Stage stage) throws Exception {

        GridPane gridPane = new GridPane();
        gridPane.setAlignment(Pos.CENTER);
        gridPane.setHgap(20);
        gridPane.setVgap(20);
        gridPane.setPadding(new Insets(25,25,25,25)); //padding top, right, bottom, left

        ToggleGroup toggleGroup = new ToggleGroup(); //one selected, other unselected
        radioButton1 = new RadioButton("Text1 Text2");
        radioButton1.setToggleGroup(toggleGroup);
        radioButton1.setSelected(true);

        radioButton2 = new RadioButton("Text2 Text1");
        radioButton2.setToggleGroup(toggleGroup);

        //add node, column-index, row-index, column-span, row-span
        gridPane.add(radioButton1, 0,0);
        gridPane.add(radioButton2, 1, 0);

        textField1 = new TextField();
        textField2 = new TextField();

        gridPane.add(new Label("Text 1: "), 0, 1);
        gridPane.add(textField1, 1, 1);
        gridPane.add(new Label("Text 2: "), 0, 2);
        gridPane.add(textField2, 1, 2);

        Button applyButton = new Button("Apply");
        applyButton.setOnAction(event -> {
            RadioButton selectedRadio = (RadioButton) toggleGroup.getSelectedToggle();

            String result;

            if (!textField1.getText().isEmpty() && !textField2.getText().isEmpty()){
                if (selectedRadio == radioButton1){
                    result = textField1.getText() + textField2.getText();
                }else {
                    result = textField2.getText() + textField1.getText();
                }

                outputLabel.setText("Results: " + result);
            }else {
                outputLabel.setText("Both Textfields must be filled");
            }

        });

        Button clear = new Button("Clear");
        clear.setOnAction(event ->{
            textField1.clear();
            textField2.clear();
            outputLabel.setText("Results: ");
        });

        HBox buttons = new HBox(10);
        buttons.setAlignment(Pos.CENTER_RIGHT);
        buttons.getChildren().addAll(applyButton, clear);
        outputLabel = new TextField("Results: ");

        VBox vbox = new VBox(10);
        vbox.getChildren().add(buttons);


        gridPane.add(outputLabel, 0,3, 2, 1);
        gridPane.add(vbox, 0, 4);

        Scene scene = new Scene(gridPane, 400, 400);
        stage.setTitle("First");
        stage.setScene(scene);
        stage.setResizable(true);
        stage.show();
    }
}
