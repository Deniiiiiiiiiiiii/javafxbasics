import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Line;
import javafx.stage.Stage;

/**
 * Created: 09.04.2023
 *
 * @author Barbos Ioana-Denisa (barbo)
 */
public class Basics_2 extends Application {

    private TextField textField1;
    private TextField textField2;
    private Label statusBar;


    @Override
    public void start(Stage stage) throws Exception {

        Label label1 = new Label("Text 1: ");
        Label label2 = new Label("Text 2: ");

        textField1 = new TextField();
        textField2 = new TextField();

        Button swap = new Button("Swap");
        Button clear = new Button("Clear");

        statusBar = new Label("Swapped");
        statusBar.setVisible(false);

        Line line = new Line();
        line.setStartX(0);
        line.setEndX(300);

        swap.setOnAction(event -> {
            String swaped = textField1.getText();
            if (!swaped.isEmpty()){
                textField1.setText(textField2.getText());
                if (!textField2.getText().isEmpty()){
                    textField2.setText(swaped);
                    statusBar.setText("Swapped");
                    statusBar.setVisible(true);
                }else {
                    statusBar.setText("Both Textfields must be filled");
                    statusBar.setVisible(true);
                    textField1.clear();
                    textField2.clear();

                }
            }else {
                statusBar.setText("Both Textfields must be filled");
                statusBar.setVisible(true);
                textField2.clear();
                textField1.clear();

            }
        });

        clear.setOnAction(event ->{
            textField1.clear();
            textField2.clear();
            statusBar.setVisible(false);
        });

        GridPane gridPane = new GridPane();
        gridPane.setAlignment(Pos.CENTER);
        gridPane.setHgap(10);
        gridPane.setVgap(10);
        gridPane.setPadding(new Insets(25, 25, 25, 25));
        gridPane.add(swap, 0, 0);
        gridPane.add(clear, 1, 0);
        gridPane.add(label1, 0, 1);
        gridPane.add(textField1, 1, 1);
        gridPane.add(label2, 0, 2);
        gridPane.add(textField2, 1, 2);


        HBox statusBarBox = new HBox(statusBar);
        statusBarBox.setAlignment(Pos.CENTER);

        VBox root = new VBox(gridPane, line, statusBarBox);
        root.setAlignment(Pos.CENTER);
        root.setSpacing(10);

        Scene scene = new Scene(root, 300, 200);
        stage.setScene(scene);
        stage.setTitle("Text Swap");
        stage.show();
    }

}
